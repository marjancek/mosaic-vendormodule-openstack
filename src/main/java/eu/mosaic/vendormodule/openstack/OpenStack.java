package eu.mosaic.vendormodule.openstack;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.openstack.api.compute.ServerResource;
import org.openstack.api.compute.ServersResource;
import org.openstack.client.ComputeClient;
import org.openstack.client.OpenStackClient;
import org.openstack.model.compute.Server;
import org.openstack.model.compute.nova.NovaAddressList.Network.Ip;
import org.openstack.model.compute.nova.NovaServerForCreate;

import eu.mosaic.vendormodule.AuthenticationException;

/**
 * 
 * @author Boris Savic
 *
 */
public class OpenStack {

	private String username;
	private String password;
	private String tenantName;
	private String endpoint;
	private String adminURL;
	private String publicURL;
	private String adminToken;
	
	private OpenStackClient openStackClient;
	
	
	/**
	 * Constructor
	 */
	public OpenStack(String username, String password, String tenantName,
						String endpoint, String adminURL, String publicURL,
						String adminToken) {
		
		this.username=username;
		this.password=password;
		this.tenantName=tenantName;
		this.endpoint=endpoint;
		this.adminURL=adminURL;
		this.publicURL=publicURL;
		this.adminToken=adminToken;
		
		try {
			connect(); //connect to server with given config
		} catch (AuthenticationException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Establish connection with OpenStack server
	 * @throws AuthenticationException 
	 */
	private void connect() throws AuthenticationException{
		
		Properties properties = new Properties(); 
		
		properties.setProperty("auth.endpoint", endpoint);
		properties.setProperty("auth.username", username);
		properties.setProperty("auth.password", password);
		properties.setProperty("auth.tenantName", tenantName);

		properties.setProperty("identity.endpoint.adminURL",adminURL);
		properties.setProperty("identity.endpoint.publicURL",publicURL);
		properties.setProperty("identity.admin.token", adminToken);
		
		try {
			openStackClient = OpenStackClient.authenticate(properties);
		}catch(Exception e){
			throw new AuthenticationException("Could not connect to OpenStack");
		}
	}
	
    /**
     * Start new virtual machine - image recognized by id. Hardware settings are stored in flavor. 
     * New VM will be named by name parameter. 
     * @param imageId - id of the virtual machine image
     * @param flavor - machine configuration (tiny, large...)
     * @param name - server name
     * @param keypair - keypair name to use
     * @return server - created server
     */
	public Server launchVirtualMachine(String imageId, String flavor, String name, String keypair) {

		try{
			ComputeClient compute = openStackClient.getComputeClient();
			
			NovaServerForCreate vm = new NovaServerForCreate();
			vm.setName(name);
			vm.setImageRef(imageId);
			vm.setFlavorRef(flavor);
			vm.setKeyName(keypair); 
			Server s = compute.createServer(vm);
			
			return s;
			
		}catch(Exception e) {
			e.printStackTrace();
			return null;
			
		}
	}
	
	/**
	 * Creates a new keypair
	 * @param name - name of the new keypair
	 * @return String containing private key of the keypair
	 */
	public String createKeypair(String name){
		try{
			ComputeClient compute = openStackClient.getComputeClient();
			return compute.createKeyPair(name).getPrivateKey();
		}catch(Exception e){
			//e.printStackTrace();
			return null;
		}
	}
		
	/**
	 * Terminate Virtual machine recognized by id
	 * @param id of the vm to stop
	 * @return
	 */
	public boolean terminateVirtualMachine(String id){
		
		try {
			ServersResource servers = openStackClient.getComputeEndpoint().servers();			
			ServerResource ourServer=servers.server(id);
			ourServer.delete();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Resume suspended VM.
	 * 
	 * @param id of the virtual machine
	 * @return
	 */
	public boolean startVirtualMachine(String id){
	
		try {
			ServersResource servers = openStackClient.getComputeEndpoint().servers();			
			ServerResource ourServer=servers.server(id);
			ourServer.resume();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Suspend/Stop Virtual machine recognized by id
	 * @param id of the vm to stop
	 * @return
	 */
	public boolean stopVirtualMachine(String id){
		
		try {
			ServersResource servers = openStackClient.getComputeEndpoint().servers();			
			ServerResource ourServer=servers.server(id);
			ourServer.suspend();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Reboot a virtual machine in OpenStack. 
	 * Note: Machine will be hard rebooted. 
	 * @param id  of the machine to reboot
	 * @return
	 */
	public boolean rebootVirtualMachine(String id){
		
		try {
			ServersResource servers = openStackClient.getComputeEndpoint().servers();			
			ServerResource ourServer=servers.server(id);
			ourServer.reboot(ServerResource.REBOOT_HARD);
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Get status of a server. 
	 * @param id - server id
	 * @return Status of a server: ACTIVE, PAUSED, ...
	 */
	public String getServerStatus(String id){
		try {
			ServersResource servers = openStackClient.getComputeEndpoint().servers();
			return servers.server(id).get().getStatus();
		}catch(Exception e){
			return null;
		}
	}
	
	/**
	 * Returns VM's privateIP address. This method may cause thread to sleep
	 * if VM's ip is currently not available.  
	 * @param vmID - ID of the VM
	 * @return
	 */
	public String getVMPrivateIP(String vmID){
		return getIp(vmID,"private");
	}
	
	/**
	 * Returns VM's publicIP address. This method may cause thread to sleep
	 * if VM's ip is currently not available.  
	 * @param vmID - ID of the VM
	 * @return
	 */
	public String getVMPublicIP(String vmID){
		return getIp(vmID,"public");
	}
	
	/**
	 * Return requested ip. ipType parameter should be "public" or "private"
	 * @param vmID - id of VM
	 * @param ipType - public or private ip
	 * @return
	 */
	private String getIp(String vmID, String ipType){
		String ip=null;
		
		try{
			ComputeClient compute = openStackClient.getComputeClient();
			Map<String, List<Ip>> ips = compute.showServer(vmID).getAddresses();
			
			//try to get ip right now, if not availabe wait for 250ms
			try{
				if((ip = ips.get(ipType).get(0).getAddr()) != null){
					return ip;
				}
			}catch (Exception e) {
			}
						
			long milisInFuture = System.currentTimeMillis()+250;
			int attemptCounter = 0;
			
			//max attempts is 20 => 20seconds of sleep is max
			while(attemptCounter<20){
				if(System.currentTimeMillis() >= milisInFuture){
					attemptCounter++;
					ips = compute.showServer(vmID).getAddresses();
					try{
						if((ip = ips.get(ipType).get(0).getAddr()) == null ){
							//ip not available yet...wait 1sec before trying again
							Thread.sleep(1000);
						}else{ //ip is available
							return ip;
						}
					}catch (Exception e) {
						Thread.sleep(1000);
					}
				}		
			}
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		
		return ip;
	}
}
