package eu.mosaic.vendormodule.openstack;

import eu.mosaic.deployment.IDeploymentResourceClass;
import eu.mosaic.resources.IResourceId;
import eu.mosaic.resources.IResourceInfo;


public class OpenStackVendorResourceInfo implements IResourceInfo {

	protected OpenStackVendorResourceId resourceId;
	private IDeploymentResourceClass deploymentClass;

	public OpenStackVendorResourceInfo(OpenStackVendorResourceId resourceId, IDeploymentResourceClass deploymentClass) {
		super();
		this.resourceId = resourceId;
		this.deploymentClass = deploymentClass;
	}

	@Override
	public IResourceId getResourceId() {		
		return resourceId;
	}

	public String toString(){
		return resourceId.toString();
	}

	@Override
	public IDeploymentResourceClass getDeploymentResourceClass() {
		return deploymentClass;
	}
}