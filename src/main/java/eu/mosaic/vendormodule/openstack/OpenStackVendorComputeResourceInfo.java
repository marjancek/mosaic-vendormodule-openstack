package eu.mosaic.vendormodule.openstack;

import eu.mosaic.deployment.IDeploymentResourceClass;
import eu.mosaic.resources.IComputeResourceInfo;

public class OpenStackVendorComputeResourceInfo 
						extends OpenStackVendorResourceInfo 
						implements IComputeResourceInfo 
{
	private String privateIP;
	private String publicIP;
	private String id;
	
	public OpenStackVendorComputeResourceInfo(OpenStackVendorResourceId resourceId, 
			IDeploymentResourceClass deploymentClass)
	{
		super(resourceId, deploymentClass);
	}
	
	public OpenStackVendorComputeResourceInfo(OpenStackVendorResourceId resourceId, 
			IDeploymentResourceClass deploymentClass, String privateIP, String publicIP) 
	{
		this(resourceId, deploymentClass);
		this.privateIP = privateIP;
		this.publicIP = publicIP;
	}

	public String getId(){
		return id;
	}
	
	@Override
	public String getPrivateIP() {
		return privateIP;
	}

	@Override
	public String getPublicIP() {
		return publicIP;
	}
	
	public void setPublicIP(String publicIP){
		this.publicIP = publicIP;
	}
	
	public void setPrivateIP(String privateIP){
		this.privateIP = privateIP;
	}
	
	public void setId(String id){
		this.id = id;
	}
	
	public String toString(){
		return super.toString() + " : " + publicIP + " / " + privateIP;
	}
}
