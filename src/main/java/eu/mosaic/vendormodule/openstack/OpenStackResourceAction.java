package eu.mosaic.vendormodule.openstack;

import eu.mosaic.vendormodule.ActionType;
import eu.mosaic.vendormodule.IResourceAction;


public class OpenStackResourceAction implements IResourceAction {
	

	private String name;
	private ActionType actionType;
	
	public OpenStackResourceAction() {		
	}
	
	public OpenStackResourceAction(String name, ActionType actionType){
		super();
		this.name = name;
		this.actionType = actionType;
	}
	
	/**
	 * Returns the name of the action
	 * 
	 * @return
	 */
	public String getName(){
		return this.name;
	}
	
	/**
	 * Returns the action type
	 * 
	 * @return
	 */
	public ActionType getActionType(){
		return this.actionType;
	}
}
