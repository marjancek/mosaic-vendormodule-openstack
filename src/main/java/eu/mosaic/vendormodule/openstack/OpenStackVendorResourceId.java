package eu.mosaic.vendormodule.openstack;

import eu.mosaic.resources.IResourceClass;
import eu.mosaic.resources.IResourceId;

public class OpenStackVendorResourceId implements IResourceId 
{

	private IResourceClass resourceClass;
	private String id;
	
	public OpenStackVendorResourceId(IResourceClass resourceClass, String id) {
		super();
		this.resourceClass = resourceClass;
		this.id = id;
	}

	@Override
	public IResourceClass getResourceClass() {
		return resourceClass;
	}

	public String getId(){
		return id;
	}
	
	public String toString(){
		return getId();
	}
}
