package eu.mosaic.vendormodule.openstack;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.openstack.model.compute.Server;

import eu.mosaic.cfp.CFP;
import eu.mosaic.deployment.ComputeDeploymentResourceClass;
import eu.mosaic.deployment.IDeploymentResourceClass;
import eu.mosaic.resources.IComputeResourceInfo;
import eu.mosaic.resources.IResourceId;
import eu.mosaic.resources.IResourceInfo;
import eu.mosaic.resources.ResourceType;
import eu.mosaic.sla.SLA;
import eu.mosaic.sla.SimpleSLA;
import eu.mosaic.sla.SlaId;
import eu.mosaic.util.remoteaccess.IFileUploader;
import eu.mosaic.util.remoteaccess.IProgramRunner;
import eu.mosaic.util.remoteaccess.SecureFileUploader;
import eu.mosaic.util.remoteaccess.SecureProgramRunner;
import eu.mosaic.vendormodule.AbstractVendorModule;
import eu.mosaic.vendormodule.AuthenticationException;
import eu.mosaic.vendormodule.CloudProviderException;
import eu.mosaic.vendormodule.IResourceAction;
import eu.mosaic.vendormodule.middleware.IVendorModuleCallback;
import eu.mosaic.vendormodule.services.log.ILoggingService;

/**
 * 
 * @author Boris Savic
 *
 */
public class OpenStackVendorModule extends AbstractVendorModule {
	
	/**Connection variables	 */
	private String username;
	private String password;
	private String tenantName;
	private String endpoint;
	private String adminURL;
	private String publicURL;
	private String adminToken;

	private int slaCounter=0;
	
	public OpenStackVendorModule(String vendorId,
			IVendorModuleCallback middlewareConnector,
			ILoggingService loggingService) 
	{
		super(vendorId, middlewareConnector, loggingService);
		
	}

	@Override
	public void doCreateResource(SlaId slaId, IDeploymentResourceClass deploymentClass)
			throws AuthenticationException, CloudProviderException {
		
		if (deploymentClass.getResourceType().equals(ResourceType.COMPUTE)) {
			ComputeDeploymentResourceClass computeClass = (ComputeDeploymentResourceClass) deploymentClass;
			String imageURL = computeClass.getImageURL(); //image ID
			
			//TODO get desired flavor (server config) and  desired vmName from computeClass or SLA
			String flavor = "1"; //temp
			String vmName = "mosaicVM"; //temp
			
			
			//TODO credentials request needed here
            if (credentialsService != null){
            		getCredentials(deploymentClass);               
                    //TODO at the moment the request is needed twice...for more info why ask Calin               
                    getCredentials(deploymentClass);
                       
                    System.out.println("Username: " + username +" \n Password: " +
                    		password + "\n TenantName: " + tenantName + "\n endpoint: " +
                    		endpoint + " \n adminURL: " + adminURL + "\n publicURL: " +
                    		publicURL + "\n token: " + adminToken
                    		);
                                                       
                    if (username == null || password == null || tenantName == null
                    		|| endpoint == null || adminURL == null || publicURL == null
                    		|| adminToken == null) {
                            throw new AuthenticationException("No credentials");
                    }                   
                    
                    OpenStackVendorComputeResourceInfo openStackComputeResourceInfo = 
                    		makeOpenStackComputeResource(
                    	    (new OpenStackVendorResourceId(deploymentClass, imageURL)), 
                                    imageURL, flavor, vmName,
                                    deploymentClass);
                    
                     if (openStackComputeResourceInfo == null){
     					onResourceCreationFailure(slaId, deploymentClass,
     							"The resource could not be created.");
     					
     				}else{
     					onResourceCreation(slaId, openStackComputeResourceInfo);
     				}
     			}else{
     				throw new AuthenticationException("Credential service not available");
     			}
     			return;
     		}
		onResourceCreationFailure(slaId, deploymentClass,
				"OPERATION NOT SUPPORTED");
	}


	/**
	 * Creates an OpenStack instance based on an image id and sets all the
	 * relevant data for that instance
	 * 
	 * @param resourceId
	 * @param imageId
	 * @return
	 */
	private OpenStackVendorComputeResourceInfo makeOpenStackComputeResource(
			OpenStackVendorResourceId resourceId,
			String imageId, String flavor, String vmName, IDeploymentResourceClass deploymentClass) {
		
		OpenStack openStack = new OpenStack(username, password, tenantName, endpoint, adminURL, publicURL, adminToken);
        
		//TODO this private key should not exist in OpenStack yet, atleast not in the first run...
		String privateKey = openStack.createKeypair("openstackkey");
		
		
		////////////////TEMPORARY/////////////////////
		BufferedWriter bw;
		try
		{
			if ( privateKey != null )
			{// TODO save the private key to a file, because I want to 
				//make a SSH connection to be sure that the uploaded file are there.
				bw = new BufferedWriter( new FileWriter("/tmp/privateOPENSTACK.key"));
				bw.write(privateKey);
				bw.close();
			}else { //if it is null read it from file
				BufferedReader in = new BufferedReader(new FileReader("/tmp/privateOPENSTACK.key"));
				
		        StringBuilder readKeyFromFile = new StringBuilder();
		        char[] buffer = new char[4096];
		        int read = 0;
		        do {
		        	readKeyFromFile.append(buffer, 0, read);
		            read = in.read(buffer);
		        } while (read >= 0);
		        privateKey = readKeyFromFile.toString();
			}
		} catch (IOException e1)
		{
			System.out.println("Can't save the private key!");
		}
		//////////////////////////////////////////////
		
		credentialsService.newKey("privateKey", privateKey, deploymentClass); //set key in credentials
		
		Server s = openStack.launchVirtualMachine(imageId, flavor, vmName, "openstackkey");
		//if launch was successful
		if(s != null){
			resourceId = new OpenStackVendorResourceId(resourceId.getResourceClass(), s.getId()); //sets id to VM id
			OpenStackVendorComputeResourceInfo computeResourceInfo 
								= new OpenStackVendorComputeResourceInfo(resourceId, deploymentClass);
			computeResourceInfo.setId(s.getId());
			
			//retrieve VM ip addresses
			String privateIp=openStack.getVMPrivateIP(s.getId());
			String publicIp=openStack.getVMPublicIP(s.getId());
			
			if(privateIp == null && publicIp == null){
				openStack.terminateVirtualMachine(s.getId()); //machine hasn't got any IP, terminate it and return null
				return null;
			}
						
			computeResourceInfo.setPrivateIP(privateIp);
			computeResourceInfo.setPublicIP(publicIp);
			
			String status = openStack.getServerStatus(s.getId());
			
			//wait until server is created and ACTIVE => max wait = 20seconds
			//this is necessary as otherwise fileUplaoder and programRunner will fail to connect
			int waitCounter=0;
			while(status.equalsIgnoreCase("active") == false){
				try {
					if(waitCounter>=20){
						openStack.terminateVirtualMachine(s.getId()); //VM hasn't gone active in 20sec, terminate it
						return null;
					}
					Thread.sleep(1000);
					waitCounter++;
					status = openStack.getServerStatus(s.getId());
				} catch (InterruptedException e) {
					e.printStackTrace();
				}			
			}
						
			return computeResourceInfo;
		}else{
			return null;
		}
	}

	@Override
	protected void doReleaseResource(IResourceId resourceId)
			throws AuthenticationException, CloudProviderException 
	{
		if (resourceId.getResourceClass().getResourceType().equals(ResourceType.COMPUTE)) {
			String id = resourceId.getId();
			
			OpenStack openStack = new OpenStack(username, password, tenantName, 
										endpoint, adminURL, publicURL, adminToken);
			openStack.terminateVirtualMachine(id);
			
			onResourceReleased(resourceId);
			return;
		}
		onResourceReleaseFailure(resourceId, "OPERATION NOT SUPPORTED");
	}

	@Override
	protected IFileUploader getFileUploader(IResourceInfo resourceInfo) 
	{
	if (resourceInfo.getResourceId().getResourceClass().getResourceType()
			.equals(ResourceType.COMPUTE)) {
			
		String privateKey = getPrivateKey(resourceInfo);
		privateKey = getPrivateKey(resourceInfo);
		if (privateKey == null){
			return null;
		}
		
		return new SecureFileUploader(((IComputeResourceInfo) resourceInfo).getPublicIP(), 22,
				privateKey, "ubuntu");
	}
	return null;
}

	
	@Override
	protected IProgramRunner getProgramRunner(IResourceInfo resourceInfo) 
	{
		if (resourceInfo.getResourceId().getResourceClass().getResourceType()
				.equals(ResourceType.COMPUTE)) {
			
			String privateKey = getPrivateKey(resourceInfo);
			privateKey = getPrivateKey(resourceInfo);
			if (privateKey == null){
				return null;
			}

			return new SecureProgramRunner(((IComputeResourceInfo) resourceInfo).getPublicIP(), 22,
					privateKey, "ubuntu");
		}
		return null;
	}
	
	/**
	 * Retrieve private key from credentialsService
	 * 
	 * @param resourceInfo
	 * @return string - private key
	 */
	private String getPrivateKey(IResourceInfo resourceInfo) {
		return credentialsService.getKey(
				"privateKey",
				resourceInfo.getDeploymentResourceClass());
	}

	@Override
	public void performAction(IResourceId resourceId, IResourceAction action)
			throws AuthenticationException, CloudProviderException 
	{
		OpenStack openStack = new OpenStack(username, password, tenantName, 
									endpoint, adminURL, publicURL, adminToken);
		
		if (resourceId.getResourceClass().getResourceType().equals(ResourceType.COMPUTE)) {
	
			String resId = resourceId.getId();

			if (action.getName().equalsIgnoreCase("START")) {
				openStack.startVirtualMachine(resId);
			} else if (action.getName().equalsIgnoreCase("STOP")) {
				openStack.stopVirtualMachine(resId);
			} else if (action.getName().equalsIgnoreCase("REBOOT")) {
				openStack.rebootVirtualMachine(resId);
			}
			return;
		}
		onResourceActionFailure(resourceId, action, "OPERATION NOT SUPPORTED");
	}

	@Override
	public void submitCFP(final CFP cfp) {
		slaCounter++;
		SLA sla = new SimpleSLA(new SlaId(getId() + "_SLA_" + (slaCounter), cfp.getId()));
		
		onProposalAvailable(sla);

	}

	@Override
	protected void ensureCredentials(SlaId slaId,
			IDeploymentResourceClass deploymentClass)
			throws AuthenticationException 
	{
			//TODO 
			String usr = credentialsService.getKey("username", deploymentClass);
			
			if(usr==null) {
				throw new AuthenticationException("No credentials");
			}
	}
	
	private void getCredentials(IDeploymentResourceClass deploymentClass) {
		
		username = credentialsService.getKey("username", deploymentClass);
        password = credentialsService.getKey("password", deploymentClass);
        tenantName = credentialsService.getKey("tenantName", deploymentClass);
        endpoint = credentialsService.getKey("endpoint", deploymentClass);
        adminURL = credentialsService.getKey("adminURL", deploymentClass);
        publicURL = credentialsService.getKey("publicURL", deploymentClass);
        adminToken = credentialsService.getKey("adminToken", deploymentClass);
        
	}
	
}
