package eu.mosaic.vendormodule.openstack;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import eu.mosaic.cfp.CFP;
import eu.mosaic.cfp.CfpId;
import eu.mosaic.cfp.ICFPResourceClass;
import eu.mosaic.deployment.ComputeDeploymentResourceClass;
import eu.mosaic.deployment.FileToDeploy;
import eu.mosaic.deployment.IDeploymentResourceClass;
import eu.mosaic.deployment.SimpleDeploymentDescriptor;
import eu.mosaic.resources.IResourceClass;
import eu.mosaic.resources.IResourceId;
import eu.mosaic.resources.IResourceInfo;
import eu.mosaic.resources.ResourceStatus;
import eu.mosaic.resources.ResourceType;
import eu.mosaic.serialization.ISerializationService;
import eu.mosaic.serialization.SerializationService;
import eu.mosaic.sla.SlaId;
import eu.mosaic.vendoragent.VendorAgent;
import eu.mosaic.vendormodule.ActionType;
import eu.mosaic.vendormodule.IResourceAction;
import eu.mosaic.vendormodule.middleware.IVendorModuleCallback;
import eu.mosaic.vendormodule.services.credentials.CredentialsService;
import eu.mosaic.vendormodule.services.deployment.DeploymentService;
import eu.mosaic.vendormodule.services.log.LoggingService;
import eu.mosaic.vendormodule.services.persistency.PersistencyService;

/**
 * Test class for OpenStack. Running this class will create new VM on openstack and
 * deploy mOSAIC platform on created VM via OpenStackVendorModule.
 * 
 */
public class OpenStackVendorModuleTest {
	
	private static final String LOGGER_NAME ="VENDOR_AGENTS_OPENSTACK";
	private static final String VENDOR_NAME = "OPENSTACK";
	private VendorAgent sampleVendor;
	private SlaId slaId;
	private CfpId cfpId;
	private IResourceClass resourceClass;
	private IResourceId resourceId;
	private int resourceCreationTryCount;
	
	
	public static void main(String[] args) throws InterruptedException {
		
		OpenStackVendorModuleTest testAgents = new OpenStackVendorModuleTest();
		testAgents.initialize();
		testAgents.start();
	}


	private void initialize() {
		//create a connector with the agents infrastructure
		IVendorModuleCallback middlewareConnector = new InfrastructureConnector();
		
		//create the services
		//serialization
		ISerializationService serializationService = new SerializationService();
		// logging
		LoggingService loggingService = new LoggingService(LOGGER_NAME);
		// persistency
		PersistencyService persistencyService = new PersistencyService(serializationService);
		//credentials
		CredentialsService credentialsService = new CredentialsService(middlewareConnector);
		
		//create a deployment service with a base directory
		String baseDirectory = "." + File.separatorChar;
		DeploymentService deploymentService = new DeploymentService(baseDirectory);

		String vendorId = VENDOR_NAME;
		
		//create a vendor module
		OpenStackVendorModule svModule = new OpenStackVendorModule(vendorId, middlewareConnector, loggingService);
		svModule.setPersistencyService(persistencyService);
		svModule.setDeploymentService(deploymentService);
		svModule.setCredentialsService(credentialsService);
		
		//create a vendor agent on the vendor module
		sampleVendor = new VendorAgent(svModule);
	}
	
	private void start() throws InterruptedException{
		
		sampleVendor.start();
		//submit a cfp
		final CfpId cfpId = new CfpId("CFP_1", "APP_1");
		sampleVendor.submitCFP(new CFP() {
			
			@Override
			public CfpId getId() {
				return cfpId;
			}

			@Override
			public String getName() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<ICFPResourceClass> getResourceClasses() {
				// TODO Auto-generated method stub
				return null;
			}
		});
		
		//create a resource
		resourceCreationTryCount = 1;
		sampleVendor.createResource(slaId, resourceClass);
		
		if (resourceId != null){

//			performRebootAction();
//			Thread.sleep(10*1000); //sleep 10sec
//			
//			performStopAction();
//			Thread.sleep(10*1000); //sleep 10sec
//			
//			performStartAction();
//			Thread.sleep(10*1000); //sleep 10sec
			
//			releaseResource();
			
		}
	}
	
	private void releaseResource(){
		sampleVendor.releaseResource(resourceId);
	}
	
	private void performRebootAction(){
		
		OpenStackResourceAction action = new OpenStackResourceAction("reboot", ActionType.REBOOT);
		sampleVendor.performAction(resourceId, action);
	}
	
	private void performStopAction(){
		OpenStackResourceAction action = new OpenStackResourceAction("stop", ActionType.STOP);
		sampleVendor.performAction(resourceId, action);
	}
	
	private void performStartAction(){
		OpenStackResourceAction action = new OpenStackResourceAction("start", ActionType.START);
		sampleVendor.performAction(resourceId, action);
	}
	
	private class InfrastructureConnector implements IVendorModuleCallback{

		
		@Override
		public void onProposalAvailable(final SlaId slaId) {
			System.out.println("<<<Proposal Available: " + slaId.toString());
			
			//Accept the proposal and create a deployment descriptor for the application
			
			//make the current accepted slaId this one
			OpenStackVendorModuleTest.this.slaId = slaId;
			
			//add a sample resource class in the deployment descriptor
			//make it global as it will be created in start() 
			IResourceClass rc = new IResourceClass() {
				
				@Override
				public ResourceType getResourceType() {
					return ResourceType.COMPUTE;
				}
				
				@Override
				public String getName() {
					return getId();
				}
				
				public String toString(){
					return getName();
				}

				@Override
				public String getId() {
					return "CLOUD_COMPUTE_TEST";
				}

				@Override
				public String getDescription() {
					return null;
				}

			};
			
			
			List<FileToDeploy> filesToDeploy = new ArrayList<FileToDeploy>();
			//get mOSAIC setup script from my DropBox
			filesToDeploy.add(new FileToDeploy("https://dl.dropbox.com/s/7pcwqe65icg2gwv/setup-mosaic.sh?dl=1", "/tmp/setup.sh"));
			
			List<String> programsToRun = new ArrayList<String>();
			//run the setup script...it will take some time to install mOSAIC
			programsToRun.add("chmod u+x /tmp/setup.sh");
			programsToRun.add("sudo /tmp/setup.sh >> log.txt 2>>log.err");
			
			String imageURL = "f33639b7-fce9-4f20-a51a-c437a0660959"; //ubuntu image ID on our OPENSTACK
			String vendorId = VENDOR_NAME;
			
			
			resourceClass = 
					new ComputeDeploymentResourceClass(slaId, rc, vendorId, imageURL, filesToDeploy, programsToRun, null);
			SimpleDeploymentDescriptor sdd = new SimpleDeploymentDescriptor(slaId, "appId", "Application Name");
			sdd.addDeploymentResourceClass((ComputeDeploymentResourceClass)resourceClass);
			try{
				sampleVendor.acceptProposal(slaId, sdd);
			}catch(Exception e){
				System.out.println("<<<Error: " + e.toString());
			}
		}

		@Override
		public void onCFPReject(CfpId cfpId, String rejectionReason) {
			System.out.println("<<<CFP Rejection: " + cfpId.toString() + ": " + rejectionReason);
			
		}

		@Override
		public void onResourceStatusUpdate(IResourceId resourceId,
				ResourceStatus status) {
			System.out.println("<<<Resource Status Update: " + resourceId.toString());
		}

		@Override
		public void onResourceActionPerformed(IResourceId resourceId,
				IResourceAction action) {
			System.out.println("<<<Resource Action: " + resourceId.toString());
		}

		@Override
		public void onResourceActionFailure(IResourceId resourceId,
				IResourceAction action, String failureReason) {
			System.out.println("<<<Resource Action Failure: " + resourceId.toString() + ": " + failureReason);
		}

		@Override
		public void onResourceCreation(SlaId slaId, IResourceId resourceId) {
			System.out.println("<<<Resource Created: " + resourceId.toString());
			OpenStackVendorModuleTest.this.resourceId = resourceId;
			IResourceInfo resourceInfo = sampleVendor.getResourceInfo(resourceId);
			System.out.println("<<<Resource Info: " + resourceInfo.toString());
		}

		@Override
		public void onResourceCreationFailure(SlaId slaId,
				IResourceClass resourceClass, String failureReason) {
			System.out.println("<<<Resource Creation Failure: " + resourceClass.toString() + ": " + failureReason);
			//try to recreate the resource 
			if (resourceCreationTryCount > 0){
				--resourceCreationTryCount;
				System.out.println("<<<Retry creating the resource: " + resourceClass.toString());
				sampleVendor.createResource(slaId, resourceClass);				
			}
		}

		@Override
		public void onResourceReleased(IResourceId resourceId) {
			System.out.println("<<<Resource Released: " + resourceId.toString());
			
		}

		@Override
		public void onResourceReleaseFailure(IResourceId resourceId,
				String failureReason) {
			System.out.println("<<<Resource Release Failure: " + resourceId.toString());
			
		}

		@Override
		public void getCredentials(String keyName, IDeploymentResourceClass resourceClass) {
			System.out.println("<<Get Credentials for: " + resourceClass.getName() +  " key: " +keyName);
			
			if (keyName.equals("username")){
				sampleVendor.onCredentials(keyName, "admin", resourceClass);
			}else if (keyName.equals("password")){
				sampleVendor.onCredentials(keyName, "584om34ifDFf01", resourceClass);
			}else if (keyName.equals("tenantName")){
				sampleVendor.onCredentials(keyName, "admin", resourceClass);
			}else if (keyName.equals("endpoint")){
				sampleVendor.onCredentials(keyName, "http://10.30.1.1:5000/v2.0", resourceClass);
			}else if (keyName.equals("adminURL")){
				sampleVendor.onCredentials(keyName, "http://10.30.1.1:35357/v2.0", resourceClass);
			}else if (keyName.equals("publicURL")){
				sampleVendor.onCredentials(keyName, "http://10.30.1.1:35357/v2.0", resourceClass);
			}else if (keyName.equals("adminToken")){
				sampleVendor.onCredentials(keyName, "584om34ifDFf01", resourceClass);
			}
		}

	}

}
